# /etc/skel/.bash_profile

# This file is sourced by bash for login shells.  The following line
# runs your .bashrc and is recommended by the bash info pages.
[[ -f ~/.bashrc ]] && . ~/.bashrc

# custom environment variables
export PATH=/usr/local/Cellar/subversion/1.7.8/bin/:/usr/local/bin:$PATH
